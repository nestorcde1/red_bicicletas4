var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas';



describe('Bicicleta API', () => {
    beforeAll((done) => { 
        mongoose.connection.close(done); 
    });
    beforeEach(function(done){
        mongoose.disconnect(done);
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database.!');
            //mongoose.disconnect();
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    afterAll(function(done){
        mongoose.connection.close(done);
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                console.log(result);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });


    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            
            var header = {'content-type': 'application/json'};
            var aBici = '{"code": 4, "color": "amarillo", "modelo": "rural", "lat": -25.549418, "lng": -54.626601}';

            request.post({
                headers: header,
                url: base_url + '/create',
                body: aBici
            }, function(error, response, body){
                if (error) {
                  return console.error('Error en el POST:', error);
                }
                expect(response.statusCode).toBe(200);
                //console.log(body);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe('amarillo');
                expect(bici.ubicacion[0]).toBe(-25.549418);
                expect(bici.ubicacion[1]).toBe(-54.626601);
                expect(bici.modelo).toBe('rural');
                done();
            });
        });
    });
    describe('POST BICICLETAS /update', () => {
        it('Status 200', (done) => {
            
            var a = new Bicicleta({code:4, color: 'rojo', modelo: 'rural', ubicacion: [-25.549418, -54.626601]});
            Bicicleta.add(a,function(err, success){
                var header = {'content-type': 'application/json'};
                var aBici2 = '{"code": 4, "color": "verde", "modelo": "rural", "lat": -25.549418, "lng": -54.626601}';

                request.put({
                    headers: header,
                    url: base_url+'/update',
                    body: aBici2
                }, function(error, response, body){
                    var resp = JSON.parse(body).bicicleta;
                    expect(response.statusCode).toBe(200);
                    expect(resp.modelo).toBe('rural');
                    expect(resp.color).toBe('verde');
                    done();
                    });
            });

            
        });
    });
    describe('POST BICICLETAS /delete', () => {
        it('Status 204', (done) => {
            var header = {'content-type': 'application/json'};
                var aBici = '{"code": 4, "color": "amarillo", "modelo": "urbano", "lat": -25.549418, "lng": -54.626601}';
                var aBici2 = '{"code": 5, "color": "verde", "modelo": "rural", "lat": -25.549418, "lng": -54.626601}';

                request.post({
                    headers: header,
                    url: base_url + '/create',
                    body: aBici
                }, function(error, response, body){
                    request.post({
                        headers: header,
                        url: base_url+'/create',
                        body: aBici2
                    }, function(error, response, body){
                        request.delete({
                            headers: header,
                            url: base_url+'/delete',
                            body: aBici
                        }, function(error, response, body){
                            expect(response.statusCode).toBe(204);
                            request.get(base_url, function(error, response, body){
                                var result = JSON.parse(body);
                                console.log(result);
                                expect(response.statusCode).toBe(200);
                                expect(result.bicicletas.length).toBe(1);
                                expect(result.bicicletas[0].code).toBe(5);
                                done();
                            });
                            done();
                        });
                    });
                });
            
        });
    });
});