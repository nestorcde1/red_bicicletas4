var expres = require('express');
var router = expres.Router();
var tokenController = require('../controllers/token');

router.get('/confirmation/:token', tokenController.confirmationGet);

module.exports = router;