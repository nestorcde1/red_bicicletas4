var mongoose = require('mongoose');
var Reserva = require('./reserva');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');   //para encriptar password
const crypto = require('crypto');
const saltRounds = 10;              //para agregarle aleatoriedad a la encriptacion
const uniqueValidator = require('mongoose-unique-validator');   //permite definir atributos UNIQUE

const Token = require('../models/token');
const mailer = require('../mailer/mailer');

const validateEmail = function(email){
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
};


var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio' ]
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        unique: true,
        lowercase: true,
        validate: [validateEmail, 'Por favor ingrese un email valido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});

//valida atributo UNIQUE
usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario'});

//para realizar cada vez que se realiza un save
usuarioSchema.pre('save', function (next) {
    //para encriptar el password si es modificado
    if(this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};


usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log('reserva antes de save: '+reserva);
    reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function (cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;

    token.save(function (err) {
        if(err) {return console.log(err.message);}

        const mailOptions = {
            from: 'no-reply@red-bici.com',
            to: email_destination,
            subject: 'Verificacion via email',
            text: 'Buenas,\n\n' + 'Por favor, para verificar su cuenta haga click en este enlace: \n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, function (err) {
            if (err) {return console.log(err.message);}

            console.log('Una verificacion de email ha sido enviada a '+email_destination);
        });
    });
};

usuarioSchema.methods.resetPassword = function (cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function (err) {
        if(err) return cb(err);
        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de password de cuenta ',
            text: 'Buenas \n\n' + 'Por favor, para resetear el password de su cuenta haga click en este enlace \n'+
                    'http://localhost:3000' + '\/resetPassword\/' + token.token + '.\n'
        };

        mailer.sendMail(mailOptions, function (err) {
            if(err) { return cb(err);}
            console.log('Se envio un email para resetear el password a ' + email_destination);
        });

        cb(null);
    });
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[
            {'googleId': condition.id}, {'email': 'nestorcde@gmail.com'}
    ]}, (err, result)=> {
            if(result){
                callback(err, resutl);
            } else {
                console.log('---------- CONDITION ------------');
                console.log(condition);
                let values = {};
                values.googleId = condition.id;
                values.email = 'nestorcde@gmail.com';
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = 'passdeprueba';
                console.log('--------- VALUES ----------');
                console.log(values);
                self.create(values, (err, result)=>{
                    if(err) {console.log(err);}
                    return callback(err, result);
                });
            }
        }
    );
};

module.exports = mongoose.model('Usuario', usuarioSchema);