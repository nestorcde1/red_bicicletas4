var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas', function () {
    beforeAll((done) => { 
        mongoose.connection.close(done); 
    });
    beforeEach(function (done) {
        mongoose.disconnect(done);
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function () {
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function (done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log('Error en el AfterEach: '+err);
            mongoose.disconnect(err); 
            done();
        });
    });

    afterAll(function(done){
        mongoose.connection.close(done);
    });

    describe('GET Bicicletas /', () => {
        it('Status 200',(done) => {
            var bici = new Bicicleta({code:1, color: 'rojo', modelo: 'deportiva', ubicacion: [-25.538694, -54.623432]});
            Bicicleta.add(bici,function(err, aBici){
                console.log(aBici);
                expect(aBici.code).toBe(1);
                expect(aBici.color).toBe('rojo');
                expect(aBici.modelo).toBe('deportiva');
                expect(aBici.ubicacion[0]).toEqual(-25.538694);
                expect(aBici.ubicacion[1]).toEqual(-54.623432);
                done();
            });            
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add',() => {
        it('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({"code" : 1, "color": 'rojo', "modelo": 'deportiva', 'ubicacion': [-25.538694, -54.623432]});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toBe(aBici.code);
                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('devolver la bici con code 3',(done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                var aBici = new Bicicleta({code: 2, color: "azul", modelo: "deportivo"});
                Bicicleta.add(aBici,function(err, newBici){
                    if(err)console.log(err);
                    var aBici2 = new Bicicleta({code: 3, color: "rosa", modelo: "rural"});
                    Bicicleta.add(aBici2,function(err, newBici){
                        if(err) console.log(err);
                        Bicicleta.findByCode(3, function(err, targetBici){
                            expect(targetBici.code).toBe(aBici2.code);
                            expect(targetBici.color).toBe(aBici2.color);
                            expect(targetBici.modelo).toBe(aBici2.modelo);
                            done();
                        });
                    });
                });
            });
        });
    });
});

/*
beforeEach(()=>{
    Bicicleta.allBicis = [];
});
describe('Bicicleta.allBicis', () => {
    it('Comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana',[-25.538694, -54.623432]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a); 
    });
});

describe('Bicicleta.findById', () => {
    it('debe devolver la bici con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var aBici = new Bicicleta(1, 'rojo', 'urbana');
        var aBici2 = new Bicicleta(2, 'verde', 'rural');
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);
    });
});

describe('Bicicleta.removeById', ()=>{
    it('debe eliminar la bici con id 2', () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var aBici = new Bicicleta(1, 'rojo', 'urbana');
        var aBici2 = new Bicicleta(2, 'verde', 'rural');
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        expect(Bicicleta.allBicis.length).toBe(2);

        Bicicleta.removeById(2);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0].id).toBe(1);
        expect(Bicicleta.allBicis[0].color).toBe(aBici.color);
        expect(Bicicleta.allBicis[0].modelo).toBe(aBici.modelo);
    });
});
*/